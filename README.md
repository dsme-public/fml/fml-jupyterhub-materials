# Fundamentals of Machine Learning

Welcome to the jupyterhub profile of the lecture Fundamentals of Machine Learning.
You can use this profile to run the notebooks provided in the lecture and exercise sessions and to solve the bonus points assignments.